resource "google_compute_health_check" "health_check" {
  name = "rmq-${var.protocol}-health-check"
  timeout_sec        = 5
  check_interval_sec = 10

  tcp_health_check {
    port = var.port
  } 
}

resource "google_compute_health_check" "ssl_health_check" {
  name = "rmq-${var.protocol}s-health-check"
  timeout_sec        = 5
  check_interval_sec = 10

  tcp_health_check {
    port = var.ssl_port
  } 
}

resource "google_compute_backend_service" "backend_services" {
  name = "rmq-${var.protocol}-backend"
  protocol = "TCP"
  load_balancing_scheme = "EXTERNAL"
  health_checks = [google_compute_health_check.health_check.id]

  dynamic "backend" {
    for_each = var.zones
    content {
      balancing_mode               = "CONNECTION"
      group                        = "https://www.googleapis.com/compute/v1/projects/pennybotdev/zones/${backend.value}/networkEndpointGroups/${var.neg_name}"
      max_connections_per_endpoint = 1000
    }
  }

  
}

resource "google_compute_backend_service" "ssl_backend_services" {
  name = "rmq-${var.protocol}s-backend"
  protocol = "TCP"
  load_balancing_scheme = "EXTERNAL"
  health_checks = [google_compute_health_check.ssl_health_check.id]
  
  dynamic "backend" {
    for_each = var.zones
    content {
      balancing_mode               = "CONNECTION"
      group                        = "https://www.googleapis.com/compute/v1/projects/pennybotdev/zones/${backend.value}/networkEndpointGroups/${var.ssl_neg_name}"
      max_connections_per_endpoint = 1000
    }
  }
}

resource "google_compute_target_tcp_proxy" "target_proxy" {
  name = "rmq-${var.protocol}-target-proxy"
  backend_service = google_compute_backend_service.backend_services.id
}

resource "google_compute_target_tcp_proxy" "ssl_target_proxy" {
  name = "rmq-${var.protocol}s-target-proxy"
  backend_service = google_compute_backend_service.ssl_backend_services.id
}

resource "google_compute_global_forwarding_rule" "forwarding_rule" {
  name = "rmq-${var.protocol}-forwarding-rule"
  ip_protocol = "TCP"
  ip_address = var.address
  load_balancing_scheme = "EXTERNAL"
  port_range = var.port
  target = google_compute_target_tcp_proxy.target_proxy.id
}

resource "google_compute_global_forwarding_rule" "ssl_forwarding_rule" {
  name = "rmq-${var.protocol}s-forwarding-rule"
  ip_protocol = "TCP"
  ip_address = var.address
  load_balancing_scheme = "EXTERNAL"
  port_range = var.ssl_port
  target = google_compute_target_tcp_proxy.ssl_target_proxy.id
}
