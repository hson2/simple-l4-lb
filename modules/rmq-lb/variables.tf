variable "zones" {
  type = list(string)
}

variable "protocol" {
  type = string
}

variable "port" {
  type = string
}

variable "ssl_port" {
  type = string
}

variable "neg_name" {
  type = string
}

variable "ssl_neg_name" {
  type = string
}

variable "address" {
  type = string
}
