variable "address_name" {
  type = string
}

variable "dns_record_name" {
  type = string
}

variable "dns_zone_name" {
  type = string
}