resource "google_compute_global_address" "ip_address" {
  name = var.address_name
}

resource "google_dns_record_set" "dns_record" {
  name = var.dns_record_name
  ttl = 300
  type = "A"

  managed_zone = data.google_dns_managed_zone.domain_zone.name
  rrdatas = [ google_compute_global_address.ip_address.address ]
}