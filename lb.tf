locals {
  asia_northeast3_zones = [
    "asia-northeast3-a",
    "asia-northeast3-c"
  ]
  us_west1_zones = [
    "us-west1-a",
    "us-west1-b",
    "us-west1-c",
  ]

  us_central1_zones = [
    "us-central1-a",
    "us-central1-b",
    "us-central1-c",
    "us-central1-f",
  ]
  
}

module "rmq_dns" {
  source = "./modules/dns"

  dns_record_name = "rmq-lb.bearrobotics.dev."
  dns_zone_name = "bearrobotics-dev"
  address_name = "rmq-lb-address"
}

module "rmq_management_lb" {
  source = "./modules/rmq-lb"

  zones = concat(local.us_central1_zones, local.us_west1_zones)
  protocol = "management"
  port = "15672"
  ssl_port = "15671"
  neg_name = "rmq-management-neg"
  ssl_neg_name = "rmq-management-tls-neg"
  address = module.rmq_dns.address
}

module "rmq_amqp_lb" {
  source = "./modules/rmq-lb"

  zones = concat(local.us_central1_zones, local.us_west1_zones)
  protocol = "amqp"
  port = "5672"
  ssl_port = "5671"
  neg_name = "rmq-amqp-neg"
  ssl_neg_name = "rmq-amqps-neg"
  address = module.rmq_dns.address
}

module "rmq_mqtt_lb" {
  source = "./modules/rmq-lb"

  zones = concat(local.us_central1_zones, local.us_west1_zones)
  protocol = "mqtt"
  port = "1883"
  ssl_port = "8883"
  neg_name = "rmq-mqtt-neg"
  ssl_neg_name = "rmq-mqtts-neg"
  address = module.rmq_dns.address
}
